import json

from django.core import serializers
from django.http import JsonResponse, HttpResponse, HttpRequest
from django.shortcuts import render
from django.template import Context, loader

import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)


def process_request_for_cors(req, resp):
    resp.__setitem__('Access-Control-Allow-Origin', '*')
    resp.__setitem__('Access-Control-Allow-Methods', '*')
    resp.__setitem__('Access-Control-Allow-Headers', '*')
    resp.__setitem__('Access-Control-Max-Age', 1728000)  # 20 days
    if req.method == 'OPTIONS':
        return JsonResponse("\n", status=200, safe=False)


def guess_name(request):
    from ipware import get_client_ip

    logger.warning(request.META)
    a = {
        "name": "mj",
        "age": 12,
        "message": "welcome",

    }
    # a.update([('age', a.get("age") + 1)])

    json = JsonResponse(a, safe=False)
    # process_request_for_cors(request, json)
    return json


def get_git_users(request):
    a = [
        {
            "text": "mh",
            "age": 12,
            "message": "welcome",
        },
        {

        }

    ]

    json = JsonResponse(a, safe=False)
    process_request_for_cors(request, json)
    return json
