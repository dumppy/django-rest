// Define a new component called button-counter
Vue.component('search-page', {
  data: function () {
    return {
      count: 0
    }
  },
  template: `e
  <template>
    <v-container>
        <v-card>
            <v-card-title>
                <v-text-field
                        v-model="search"
                        append-icon="search"
                        label="Search"
                        single-line
                        hide-details
                ></v-text-field>
            </v-card-title>
            <v-data-table
                    :headers="headers"
                    :items="desserts"
                    :search="search"
            ></v-data-table>
        </v-card>
    </v-container>
</template>
`
})


