Vue.component('search-page',
   {
    template: '<p>Hello I am A</p>'
    })
 var vue = new Vue(
    {
        el: '#app',
        vuetify: new Vuetify(),
        delimiters: ["[[", "]]"],
        data: {
            name: " ",
            message: 'Hello Vue!'
        },
        mounted() {
            var config = {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Methods': '*'
                }
            };
            axios.get('http://localhost:8000/guess_name')
                .then(response =>
               {
               this.message = response.data.message
               this.name = response.data.name

               })
        }
    }
)