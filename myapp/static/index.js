
// Define a new component called button-counter
var searchPage = {
      data () {
     return {

        search: '',
        user: [],
        headers: [
          {
            text: 'Dessert (100g serving)',
            align: 'start',
            filterable: false,
            value: 'name',
          },
          { text: 'Calories', value: 'calories' },
          { text: 'Fat (g)', value: 'fat' },
          { text: 'Carbs (g)', value: 'carbs' },
          { text: 'Protein (g)', value: 'protein' },
          { text: 'Iron (%)', value: 'iron' },
        ],
        desserts: [
          {
            name: 'Frozen Yogurt',
            calories: 159,
            fat: 6.0,
            carbs: 24,
            protein: 4.0,
            iron: '1%',
          },
          {
            name: 'Ice cream sandwich',
            calories: 237,
            fat: 9.0,
            carbs: 37,
            protein: 4.3,
            iron: '1%',
          },
          {
            name: 'Eclair',
            calories: 262,
            fat: 16.0,
            carbs: 23,
            protein: 6.0,
            iron: '7%',
          },
          {
            name: 'Cupcake',
            calories: 305,
            fat: 3.7,
            carbs: 67,
            protein: 4.3,
            iron: '8%',
          },
          {
            name: 'Gingerbread',
            calories: 356,
            fat: 16.0,
            carbs: 49,
            protein: 3.9,
            iron: '16%',
          },
          {
            name: 'Jelly bean',
            calories: 375,
            fat: 0.0,
            carbs: 94,
            protein: 0.0,
            iron: '0%',
          },
          {
            name: 'Lollipop',
            calories: 392,
            fat: 0.2,
            carbs: 98,
            protein: 0,
            iron: '2%',
          },
          {
            name: 'Honeycomb',
            calories: 408,
            fat: 3.2,
            carbs: 87,
            protein: 6.5,
            iron: '45%',
          },
          {
            name: 'Donut',
            calories: 452,
            fat: 25.0,
            carbs: 51,
            protein: 4.9,
            iron: '22%',
          },
          {
            name: 'KitKat',
            calories: 518,
            fat: 26.0,
            carbs: 65,
            protein: 7,
            iron: '6%',
          },
        ],

      }

  },
    mounted() {
            var config = {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Methods': '*'
                }
            };
            axios.get('http://localhost:8000/get_git_users')
                .then(response =>
               {
               this.headers = response.data.headers
               this.desserts  =  response.data.users
               })
        },

       methods: {
           greet: function (event) {
           console.log(this.search)
           axios.get('http://localhost:8000/get_git_users/' + this.search)
                .then(response =>
               {
               this.headers = response.data.headers
               this.user  =  response.data.user
               })
    }
  } ,
  template: `

    <v-container>
        <v-card>
            <v-card-title>
                <v-text-field
                        v-on:keyup.enter.prevent="greet"
                        v-model="search"
                        append-icon="search"
                        label="Search"
                        single-line
                        hide-details
                ></v-text-field>
            </v-card-title>
            <v-data-table
                    :headers="headers"
                    :items="desserts"
                    :search="search"
            ></v-data-table>
        </v-card>
//         card image
  <v-card
    class="mx-auto"
    max-width="400"
  >
    <v-img
      class="white--text align-end"
      height="200px"
      :src="this.user.avatar_url"
    >
      <v-card-title> {{ this.user.login }} </v-card-title>
    </v-img>

    <v-card-subtitle class="pb-3"> Followers  {{ this.user.followers }}</v-card-subtitle>
    <v-card-subtitle class="pb-3"> Followings  {{ this.user.following }}</v-card-subtitle>
    <v-card-subtitle class="pb-3"> Repositories  {{ this.user.public_repos }}</v-card-subtitle>

    <v-card-text class="text--primary">
      <div> my name  is    {{ this.user.name }}</div>

      <div>signup  'in'  {{ this.user.created_at }}   </div>
    </v-card-text>

    <v-card-actions>
      <v-btn
        color="orange"
        text
      >
        Share
      </v-btn>

      <v-btn
        :href="this.user.html_url"
        color="orange"
        text
      >
        Explore
      </v-btn>
    </v-card-actions>
  </v-card>
    </v-container>

`
}

var vue = new Vue(
    {
        el: '#app',
          components: {
               'search-page': searchPage
        },
        vuetify: new Vuetify(),
        delimiters: ["[[", "]]"],
        data: {
            name: null,
            message: 'Hello Vue!'
        },

    }
)

