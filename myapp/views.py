from django.http.response import JsonResponse
from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, permissions
from myapp.serializers import UserSerializer, GroupSerializer
import requests
import logging
import json


# Create your views here.

def index(request):
    return render(request, 'index.html')


def get_git_users(request):
    import json
    github_response = requests.get("https://api.github.com/users")
    response = json.loads(github_response.content)
    headers = list(map(lambda n1: {"text": n1, "value": n1}, response[0]))
    users = list(map(lambda n1: n1, response))

    # logging.warning(users)
    a = {
        'headers': headers,
        'users': users
    }

    return JsonResponse(a, safe=False)


def get_git_user_info(request, slug):
    import json
    github_response = requests.get("https://api.github.com/users/" + slug)
    response = json.loads(github_response.content)
    headers = list(map(lambda n1: {"text": n1, "value": n1}, response))
    user = response

    logging.warning(response)
    a = {
        'headers': headers,
        'user': user

    }

    return JsonResponse(a, safe=False)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]
