from django.urls import path, include
from django.conf.urls import url
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)

urlpatterns = [
    path('', views.index, name='index'),
    path('', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^get_git_users/$', views.get_git_users, name='get_git_users'),
    path('get_git_users/<slug:slug>', views.get_git_user_info, name='get_git_users'),

    path('polls/', include('polls.urls')),
]
